Made a quick tic tac toe game

- basic react and CSS to allow a user to play tictactoe.

- installed cypress E2E testing
    - open repo
    - npm i
    - npm start
    - open new terminal
    - npx cypress open
        - e2e testing
        - run reset_test spec
        - run win_test spec