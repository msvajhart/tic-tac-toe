import React from 'react';
import './static/style/main.css';

class App extends React.Component {
  render() {
    return (
      <>
        <Game/>
      </>
    )
  }
}

const Game = () => {
  //initial values
  const [gameValues, setGameValues] = React.useState([
    ["","",""],
    ["","",""],
    ["","",""]
  ]);
  const [player, setPlayer] = React.useState("X");
  const [gameState, setGameState] = React.useState("playing")

  //update the clicked box with the current player
  const updateValue = (row, column) => {
    //check if box has a value and that we are still playing
    if (gameValues[row][column] !== "" || gameState !== "playing") return;

    //add player to clicked box
    let newValues = gameValues;
    newValues[row][column] = player;
    setGameValues(newValues)

    //switch to next player
    player === "X" ? setPlayer("O") : setPlayer("X");

    //check if this action created a win condition
    checkForWin();
  }

  const checkForWin = () => {
    //check rows
    for(let i = 0; i < gameValues.length; i++){
      //make sure the value we are checking isn't blank
      if(gameValues[i][0] === "") continue;


      //check if every value in the array is the same as the first value
      if(gameValues[i].every((value) => value === gameValues[i][0])){
        setGameState("Winner: " + gameValues[i][0]);
        break;
      }
    }

    //check column
    //loop through columns and make sure each value is the same as the first
    for(let i = 0; i < gameValues.length; i++){
        if( gameValues[0][i] !== "" && (gameValues[0][i] === gameValues[1][i] && gameValues[0][i] === gameValues[2][i])){
          setGameState("Winner: " + gameValues[0][i]);
          break;
        }
    }

    //check diagonals
    //check if the corners are the same as the center item
    if((gameValues[1][1] !== "" && gameValues[1][1] === gameValues[0][0] && gameValues[1][1] === gameValues[2][2]) || (gameValues[1][1] !== "" && gameValues[1][1] === gameValues[2][0] && gameValues[1][1] === gameValues[0][2])){
        setGameState("Winner: " + gameValues[1][1]);
    }


    //check tie
    //assume its a tie unless proven otherwise
    let tie = true;
    //loop through each item
    for(let i = 0; i < gameValues.length; i++){
      for(let j = 0; j < gameValues.length; j++){
        //if the value is available then it cant be a tie
        if(gameValues[i][j] === "") {
          tie = false; 
          break;
        }
      }
      //if we found out its a tie stop looking
      if(tie === false){
        break;
      } 
    }
    //declare a tie
    if(tie) setGameState("Tie");
  }

  //reset to initial state
  const reset = () => {
    setGameState("playing")
    setPlayer("X");
    setGameValues([
      ["","",""],
      ["","",""],
      ["","",""]
    ])
  }

  return (
    <>
    
        {
          gameState === "playing" ? 
            <div className={"status"}>Next player: {player} </div>
            : <div className={"status"}>{gameState}</div>
        }
      <button onClick={reset} class="reset">Reset</button>
    <div id="game">
      <div className='row'>
        <Square row={0} column={0} value={gameValues} setValue={updateValue}/>
        <Square row={0} column={1} value={gameValues} setValue={updateValue}/>
        <Square row={0} column={2} value={gameValues} setValue={updateValue}/>
      </div>
      <div className='row'>
        <Square row={1} column={0} value={gameValues} setValue={updateValue}/>
        <Square row={1} column={1} value={gameValues} setValue={updateValue}/>
        <Square row={1} column={2} value={gameValues} setValue={updateValue}/>
      </div>
      <div className='row'>
        <Square row={2} column={0} value={gameValues} setValue={updateValue}/>
        <Square row={2} column={1} value={gameValues} setValue={updateValue}/>
        <Square row={2} column={2} value={gameValues} setValue={updateValue}/>
      </div>
    </div>
    </>
  )
}

const Square = ({row, column, value, setValue}) => {
  return (
    <div className={value[row][column] === "" ? "square" : "square taken"} id={`square${row}-${column}`} onClick={()=>{setValue(row,column)}}>
        {value[row][column]}
    </div>
  )
}


export default App