describe('Row Win X Spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:3000/')
    cy.get("#game").find('#square0-0').click()
    cy.get("#game").find('#square1-1').click()
    cy.get("#game").find('#square0-1').click()
    cy.get("#game").find('#square1-2').click()
    cy.get("#game").find('#square0-2').click()
    cy.get("body").find(".status").contains("Winner: X")
  })
})



describe('Row Win O Spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:3000/')
    cy.get("#game").find('#square0-0').click()
    cy.get("#game").find('#square1-1').click()
    cy.get("#game").find('#square0-1').click()
    cy.get("#game").find('#square1-2').click()
    cy.get("#game").find('#square2-2').click()
    cy.get("#game").find('#square1-0').click()
    cy.get("body").find(".status").contains("Winner: O")
  })
})

describe('Column Win X Spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:3000/')
    cy.get("#game").find('#square0-0').click()
    cy.get("#game").find('#square1-1').click()
    cy.get("#game").find('#square1-0').click()
    cy.get("#game").find('#square1-2').click()
    cy.get("#game").find('#square2-0').click()
    cy.get("body").find(".status").contains("Winner: X")
  })
})

describe('Column Win O Spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:3000/')
    cy.get("#game").find('#square0-0').click()
    cy.get("#game").find('#square0-1').click()
    cy.get("#game").find('#square0-2').click()
    cy.get("#game").find('#square1-1').click()
    cy.get("#game").find('#square2-2').click()
    cy.get("#game").find('#square2-1').click()
    cy.get("body").find(".status").contains("Winner: O")
  })
})


describe('Diaganol Win O Spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:3000/')
    cy.get("#game").find('#square1-1').click()
    cy.get("#game").find('#square0-1').click()
    cy.get("#game").find('#square0-0').click()
    cy.get("#game").find('#square2-1').click()
    cy.get("#game").find('#square2-2').click()
    cy.get("body").find(".status").contains("Winner: X")
  })
})


describe('Diagonal Win O Spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:3000/')
    cy.get("#game").find('#square0-0').click()
    cy.get("#game").find('#square1-1').click()
    cy.get("#game").find('#square2-2').click()
    cy.get("#game").find('#square0-2').click()
    cy.get("#game").find('#square0-1').click()
    cy.get("#game").find('#square2-0').click()
    cy.get("body").find(".status").contains("Winner: O")
  })
})